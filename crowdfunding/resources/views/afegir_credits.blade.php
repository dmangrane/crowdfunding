@extends('layouts.app')

@section('content')
    <p>En aquest moment tens {{ $credits->credits }} credits. Quants en vols afegir?</p>
    <form action="/compra_credits" method="post">
        @csrf
        <input type="number" name="credits" id="credits">
        <input type="submit" value="Afegir">
    </form>
@endsection
