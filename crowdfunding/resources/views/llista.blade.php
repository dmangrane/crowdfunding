@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    <h3>LLISTAT DE PROJECTES</h3>
    @if(count($projectes)>0)
    <table>
       <thead>
        <tr>
            <th>Nom:</th>
            <th>Descripció</th>
            <th>Quantitat objectiu</th>
            <th>Quantitat recollida:</th>
            <th>Quantitat restant:</th>
        </tr>
       </thead>
       <tbody>
        @foreach($projectes as $i)
        <tr>
            <td><a href="/detall_projecte?id={ $i->id }}">{{ $i->nom }}</a></td>
            <td>{{ $i->descripcio }}</td>
            <td>{{ $i->objectiu }}</td>
            <td>{{ $i->donatius }}</td>
            <td>{{ ($i->objectiu)-$i->donatius }}</td>
        </tr>
        @endforeach
       </tbody>
    </table>
    @else
        <p>Encara no hi ha cap Projecte</p>
        <button><a href="/projectes">Crea un nou projecte</a></button>
        <button<a href="/credits">Consulta els teus credits</a></button>
    </div>
</div>
@endsection