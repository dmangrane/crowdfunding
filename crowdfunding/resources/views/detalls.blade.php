@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    <h3>PROJECTE</h3>
    <table>
       <thead>
        <tr>
            <th>Nom:</th>
            <th>Descripció</th>
            <th>Quantitat objectiu</th>
            <th>Quantitat recollida:</th>
        </tr>
       </thead>
       <tbody>
        <tr>
            <td>{{ $detalls->nom }}</a></td>
            <td>{{ $detalls->descripcio }}</td>
            <td>{{ $detalls->objectiu }}</td>
            <td>{{ $detalls->donatius }}</td>
        </tr>
        <tr>
            <td><a href="/nou_donatiu?id={{ $detalls->id }}"><Fer un donatiu</a></td>
        </tr>
       </tbody>
    </table>
    </div>
</div>
@endsection