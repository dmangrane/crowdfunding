@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    <h3>CREA UN PROJECTE NOU</h3>
        <form action="/crear_projectes" method="post">
            @csrf
            @method('POST')
            <table>
                <tr>
                    <td><label for="nom">Nom:</label></td>
                    <td><input type="text" name="nom" id="nom"></td>
                </tr>
                <tr>
                    <td><label for="descripcio">Descripcio:</label></td>
                    <td><input type="text" name="descripcio" id=""descripcio"></td>
                </tr>
                <tr>
                    <td><label for="objectiu">Objectiu:</label></td>
                    <td><input type="text" name="objectiu" id="objeciu"></td>
                </tr>
            </table>
            <input type="submit" value="Enviar">
        </form>
    </div>
</div>
@endsection