@extends('layouts.app')

@section('content')
    <h1>Donatiu pel projecte {{ $projecte->nom }}</h1>
    <form action="/afegir_diners" method="post">
        @csrf
        <label for="donatius">Quantitat a donar</label>
        <input type="number" name="donatius" id="donatius">
        <input type="submit" value="Afegir">
    </form>
@endsection
