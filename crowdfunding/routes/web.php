<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Init@start')->name('llista');

Route::get('projectes','Init@projectes')->middleware('auth');

Route::post('crear_projectes','Init@crear_projectes')->middleware('auth');

Route::get('detall_projecte','Init@detall_projecte')->middleware('auth');

Route::get('credits','Init@credits')->middleware('auth');

Route::post('afegir_credits','Init@compra_credits')->middleware('auth');

Route::get('nou_donatiu','Init@nou_donatiu')->middleware('auth');

Route::post('afegir_diners','Init@afegir_diners')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
