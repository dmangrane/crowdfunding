<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projecte;
use App\Credits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Init extends Controller {

  public function start(){
    $projectes = Projecte::all();
    return view ('llista',['projectes'=>$projectes]);
   } 

  public function projectes(){
      return view('crea_projectes');
  } 

   public function detall_projecte(Request $req){
        $detalls = Projecte::find($req->$id);
        return view('detalls',['detalls'=>$detalls]);
   }
  
   public function crear_projectes(Request $req){
        $nom = $req->input('nom');
        $descripcio = $req->input('descripcio');
        $objectiu = $req->input('objectiu');

        $projecte = Projecte::find(1);

        if(empty($projecte)) $projecte = new Projecte;

         $projecte->nom = $nom;
         $projecte->descripcio = $descripcio;
         $projecte->objectiu = $objectiu;
         $projecte->donatius = 0;

         $projecte->save();

         return redirect()->route('llista');

   }

   public function credits(Request $req){
    $userid = Auth::id();
    try {
        $credits = Credits::findOrFail($userid);
    } catch (ModelNotFoundException $ex){
        $credits = new credits;
        $credits->id=$userid;
        $credits->credits=0;
        $credits->save();
    } finally {
        return view('afegir_credits',['credits'=>$credits]);
    }
}

public function compra_credits(Request $req){
    $userid = Auth::id();
    $credits = Credits::find($userid);
    $credits->credits=$credits->credits+$req->input('credits');
    $credits->save();
    return redirect()->route('llista');
}

   public function nou_donatiu(Request $req){
       $projecte = Projecte::find($req->$id);
       return view('nou_donatiu',['projecte'=>$projecte]);
   }

   public function afegir_diners(Request $req){
       try{
        $projecte = Projecte::find($req->$id);
        $userid = Auth::id();
        $afegir = $req->input('donatius');
            $credits = Credits::findOrFail($userid);
            if ($credits->credits>=$toadd){
                DB::beginTransaction();
                $projecte->donatius=$projecte->donatius+$afegir;
                $projecte->save();
                $credits->credits=$credits->credits-$afegir;
                $credits->save();
                DB::commit();
                return view('llista');
            } else {
                return view('error');
            }
        } catch (ModelNotFoundException $ex) {
            return view('llista');
        }
    }
}
